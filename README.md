# ![GitLab CI Job Prototypes icon](icon.m.png) GitLab CI Job Prototypes

A library of reusable GitLab CI jobs for NPM users

## Usage

1. `npm install --save-dev gitlabci-job-prototypes` - will create a gitlabci-job-prototypes folder in your project
2. use in .gitlab-ci.yml via https://docs.gitlab.com/ee/ci/yaml/#include

More details in documentation below

## Documentation 

[Usage docs](./gitlabci-job-prototypes/README.md)

The same documentation is available to you with the installed package.

## Sponsor

This project originates from and is sponsored by 

[![Egnyte Inc.](http://egnyte.com/assets/2015/images/logo.png)](https://egnyte.com/)

## License

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

## CHANGELOG

### 1.0.0
Adopt `extend` as the reuse mechanism.
Introduce npm dependencies jobs
