# Usage

1. install dev dependency

```
 npm install --save-dev gitlabci-job-prototypes
```

The package will create a folder called gitlabci-job-prototypes in the repo root, commit that.
The folder will also contain a VERSION file to track which version you have. When npm install updates the package it will overwrite the whole folder.

When you want to pull new changes
```
 npm update gitlabci-job-prototypes
```

2. use in gitlab-ci.yml via https://docs.gitlab.com/ee/ci/yaml/#include

```yml
include:
  - "/gitlabci-job-prototypes/dependencies.yml"
  - "/gitlabci-job-prototypes/build.yml"

dependencies-dev:
  variables:
    CURRENT_NODE_ALPINE: "node:10-alpine"
  extends: .s-setup-dependencies-dev
  before_script:
    - your custom modification
```

## Catalog of reusable jobs

### dependencies.yml

| job                                | produces                                                                                                                                    | requires variables                                             |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| .s-setup-dependencies-dev          | creates an artifact with node_modules to depend on in testing jobs                                                                          | CURRENT_NODE_ALPINE                                   |
| .s-setup-dependencies-prod         | creates an artifact with node_modules to depend on in production build, checks the audit                                                    | CURRENT_NODE_ALPINE                                   |
| .s-setup-dependencies-prv-ssh-dev  | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in testing jobs                       | CURRENT_NODE_ALPINE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |
| .s-setup-dependencies-prv-ssh-prod | with support for private packages over ssh to git; creates an artifact with node_modules to depend on in production build, checks the audit | CURRENT_NODE_ALPINE SSH_PRIVATE_KEY_BASE64 SSH_CONFIG |

### build-gcr.yml

| job              | produces                                                                                                                             | requires                                                                                 |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| .s-build-image   | builds and uploaads an image gcr.io/${GCR_CONTAINER_REGISTRY_NAME}/${BUILD_APP_NAME} and tags it with "latest" and \${CI_COMMIT_SHA} | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64                 |
| .s-tag-named-tag | tags image gcr.io/${GCR_CONTAINER_REGISTRY_NAME}/${BUILD_APP_NAME}:${CI_COMMIT_SHA} with${BUILD_NAMED_TAG}                           | BUILD_APP_NAME GCR_CONTAINER_REGISTRY_NAME DOCKER_WRITER_GCR_JSON_BASE64 BUILD_NAMED_TAG |

when using .s-build-image you must override `dependencies` param and pass the name of the job which produced production node_modules artefact

```yml
include:
  - "/gitlabci-job-prototypes/dependencies.yml"
  - "/gitlabci-job-prototypes/build.yml"

dependencies-prod:
  variables:
    CURRENT_NODE_ALPINE: "node:10-alpine"
  extends: .s-setup-dependencies-prod

build-image:
  dependencies:
    - dependencies-prod
  extends: .s-build-image
  variables:
    BUILD_APP_NAME: "my-app"
    ...

tag-master:
  only:
    - master
  stage: after_test
  variables: 
    BUILD_APP_NAME: "my-app"
    BUILD_NAMED_TAG: "master"
  extends: .s-tag-named-tag

### utils-gcr.yml

| job              | produces                                                                                                                             | requires                                                                                 |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------- |
| .s-bucket-upload   | uploads a given file to a bucket cp ${LOCAL_FILE} gs://${DESTINATION_BUCKET} |  LOCAL_FILE DESTINATION_BUCKET  GC_WRITER_JSON_BASE64 |

`LOCAL_FILE` can be a parh with glob syntax (google cloud sdk supports that)

```yml
deploy-page:
  extends: .s-bucket-upload
  variables:
    GC_WRITER_JSON_BASE64: "credentials for writing the bucket"
    LOCAL_FILE: "index.html"
    DESTINATION_BUCKET: "websitebucket/website/main"
```

```yml
deploy-page:
  extends: .s-bucket-upload
  variables:
    GC_WRITER_JSON_BASE64: "credentials for writing the bucket"
    LOCAL_FILE: "*.html"
    DESTINATION_BUCKET: "websitebucket/website/main"
```