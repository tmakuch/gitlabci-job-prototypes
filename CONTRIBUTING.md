# Contributing

The goal of this project is to be a library of reusable jobs, so we're happy to see you contribute yours!

## Obvious choices for contributions
- support for various docker registries 
- support for cloud deployments
- usage of GitLab features

## Adding a job

Before you contribute go through these steps:
- Check if the job you want to contribute is not specific to your narrow use case
- Open an issue to discuss your idea before you spend time building it

Making the contribution
- Create a merge request with the yaml file and a section in usage doc
- Make sure you don't leave any hardcoded values specific to your setup
- In the merge request description explain what the job does and why it should be added to the library
- Add an example of usage

